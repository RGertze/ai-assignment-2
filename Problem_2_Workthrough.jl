### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 56ba85ce-899d-4f54-9916-35835fdd1f42
begin
	mutable struct CSP <: AbstractCSP
		vars::AbstractVector
		domains::CSPDict
		neighbors::CSPDict
		constraints::Function
		initial::Tuple
		current_domains::Union{Nothing, Dict}
		nassigns::Int64
	
	    function CSP(vars::AbstractVector, domains::CSPDict, neighbors::CSPDict, constraints::Function;
	                initial::Tuple=(), current_domains::Union{Nothing, Dict}=nothing, nassigns::Int64=0)
	        return new(vars, domains, neighbors, constraints, initial, current_domains, nassigns)
	    end
	end
	
	"""
	    assign(problem, key, val, assignment)
	Overwrite (if an value exists already) assignment[key] with 'val'.
	"""
	function assign(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
	    assignment[key] = val;
	    problem.nassigns = problem.nassigns + 1;
	    nothing;
	end
	
	"""
	    unassign(problem, key, val, assignment)
	Delete the existing (key, val) pair from 'assignment'.
	"""
	function unassign(problem::T, key, assignment::Dict) where {T <: AbstractCSP}
	    if (haskey(assignment, key))
	        delete!(assignment, key);
	    end
	    nothing;
	end
	
	function nconflicts(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
	    return count(
	                (function(second_key)
	                    return (haskey(assignment, second_key) &&
	                        !(problem.constraints(key, val, second_key, assignment[second_key])));
	                end),
	                problem.neighbors[key]);
	end
	
	function display(problem::T, assignment::Dict) where {T <: AbstractCSP}
	    println("CSP: ", problem, " with assignment: ", assignment);
	    nothing;
	end
end

# ╔═╡ e51613b0-ccbc-11ec-2ca7-e7aeb8397747
begin
	import Base: get, getindex, getkey,
	            deepcopy, copy, haskey,
	            in, display;
	
	export ConstantFunctionDict, CSPDict, CSP, NQueensCSP, SudokuCSP, ZebraCSP,
	    get, getkey, getindex, deepcopy, copy, haskey, in,
	    assign, unassign, nconflicts, display, infer_assignment,
	    MapColoringCSP, backtracking_search, parse_neighbors,
	    AC3, first_unassigned_variable, minimum_remaining_values,
	    num_legal_values, unordered_domain_values, least_constraining_values,
	    no_inference, forward_checking, maintain_arc_consistency,
	    min_conflicts, tree_csp_solver, topological_sort,
	    support_pruning, solve_zebra;
end

# ╔═╡ 03820ab4-61d6-480a-a31f-25f98074ad18
begin
	struct ConstantFunctionDict{V}
	    value::V
	
	    function ConstantFunctionDict{V}(val::V) where V
	        return new(val);
	    end
	end
	
	ConstantFunctionDict(val) = ConstantFunctionDict{typeof(val)}(val);
	
	copy(cfd::ConstantFunctionDict) = ConstantFunctionDict{typeof(cfd.value)}(cfd.value);
	
	deepcopy(cfd::ConstantFunctionDict) = ConstantFunctionDict{typeof(cfd.value)}(deepcopy(cfd.value));
	
	mutable struct CSPDict
	    dict::Union{Nothing, Dict, ConstantFunctionDict}
	
	    function CSPDict(dictionary::Union{Nothing, Dict, ConstantFunctionDict})
	        return new(dictionary);
	    end
	end
	
	function getindex(dict::CSPDict, key)
	    if (typeof(dict.dict) <: ConstantFunctionDict)
	        return dict.dict.value;
	    else
	        return getindex(dict.dict, key);
	    end
	end
	
	function getkey(dict::CSPDict, key, default)
	    if (typeof(dict.dict) <: ConstantFunctionDict)
	        return dict.dict.value;
	    else
	        return getkey(dict.dict, key, default);
	    end
	end
	 
	function get(dict::CSPDict, key, default)
	    if (typeof(dict.dict) <: ConstantFunctionDict)
	        return dict.dict.value;
	    else
	        return get(dict.dict, key, default);
	    end
	end
	
	function haskey(dict::CSPDict, key)
	    if (typeof(dict.dict) <: ConstantFunctionDict)
	        return true;
	    else
	        return haskey(dict.dict, key);
	    end
	end
	
	function in(pair::Pair, dict::CSPDict)
	    if (typeof(dict.dict) <: ConstantFunctionDict)
	        if (getindex(pair, 2) == dict.dict.value)
	            return true;
	        else
	            return false;
	        end
	    else
	        #Call in() function from dict.jl(0.5)/associative.jl(0.6~nightly).
	        return in(pair, dict.dict);
	    end
	end
	
	abstract type AbstractCSP <: AbstractProblem end;
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═e51613b0-ccbc-11ec-2ca7-e7aeb8397747
# ╠═03820ab4-61d6-480a-a31f-25f98074ad18
# ╠═56ba85ce-899d-4f54-9916-35835fdd1f42
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
